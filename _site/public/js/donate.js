/**
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    var animateCircle = function(context, currentPct, endPct) {
        var elDims = 250;
        var centerPoint = elDims / 2;
        var radius = centerPoint - 5;
        var fullCircle = Math.PI * 2;
        var quarterCircle = fullCircle / 4;
        
        context.clearRect(0, 0, elDims, elDims);
        context.lineWidth = 5;

        context.strokeStyle = '#E7E7E7';
        context.beginPath();
        context.arc(centerPoint, centerPoint, radius, 0, fullCircle, false);
        context.stroke();

        context.strokeStyle = '#DB8F27';
        context.beginPath();
        context.arc(centerPoint, centerPoint, radius, -quarterCircle, (fullCircle * currentPct) - quarterCircle, false);
        context.stroke();

        if ((currentPct += .01) < endPct) {
            requestAnimationFrame(function () {
                animateCircle(context, currentPct, endPct);
            });
        }
    }

    $('.goal-progress-circle').each(function(index, canvas) {
        animateCircle(canvas.getContext('2d'), 0, $(canvas).data('progress'));
    });

    $('.copy-address-btn').click(function() {
        var $btn = $(this);
        var $input = $btn.parents('.donation-addresses-container')
            .find('.donation-address-input');

        // thanks https://stackoverflow.com/a/41267511
        if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
            var el = $input.get(0);
            el.contentEditable = true;
            var range = document.createRange();
            range.selectNodeContents(el);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
            el.setSelectionRange(0, 999999);
            el.contentEditable = false;
        } else {
            $input.select();
        }
        document.execCommand('copy');
        
        $btn.tooltip({trigger: 'manual'})
            .tooltip('show')
            .on('hidden.bs.tooltip', function() { $btn.tooltip('dispose'); });

        setTimeout(function() { $btn.tooltip('hide'); }, 1000);
    });

    $('.donation-address-input').click(function() {
        $(this).select();
    });
});