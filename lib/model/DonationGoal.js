/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

/**
 * Represents a donation goal
 */
module.exports = class DonationGoal {
    /**
     * Creates a `DonationGoal` object from a POJsO
     * 
     * @param {object} rawGoal The POJsO to pull data from
     * @param {array} acceptedCryptos The cryptos to walk through and grab details from
     */
    constructor(rawGoal, acceptedCryptos) {
        this.id = rawGoal.id;
        this.name = rawGoal.name;
        this.description = rawGoal.description;
        this.group = rawGoal.goal_type;
        this.daysRemaining = rawGoal.days_remaining;
        this.completedAt = rawGoal.completedAt;
        this.donations = rawGoal.donations;
        this.goalBTCTotal = rawGoal.goal_btc_total;
        this.currentBTCTotal = rawGoal.current_btc_total;
        this.cryptos = {};
        this.updatedAt = rawGoal.updated_at;

        acceptedCryptos.forEach(crypto => {
            this.cryptos[crypto] = {
                address: rawGoal[`${crypto}_address`], 
                balance: parseFloat(rawGoal[`${crypto}_balance`]) 
            };
        });
    }
}