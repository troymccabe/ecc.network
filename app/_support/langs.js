/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

/**
 * The list of languages that are currently supported
 */
module.exports = {
    ae: 'العَرَبِيَّة‎',
    zh_hans: '简体中文',
    zh_hant: '中国传统的',
    en: 'English',
    es: 'Español',
    fr: 'Français',
    de: 'Deutsch',
    id: 'bahasa Indonesia',
    kr: '조선말/한국어',
    nl: 'Nederlands',
    pl: 'Polski',
    // pt_br: 'Português (BR)',
    pt: 'Português',
    tr: 'Türkçe'
};