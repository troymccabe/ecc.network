/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

const CoinMarketCap = require('node-coinmarketcap');
const MarketDetail = require('../model/MarketDetail');

/** 
 * The market detail service retrieves market details for cryptocurrencies
 */
module.exports = class MarketDetailService {
    /**
     * Retrieves market details for specified cryptocurrencies
     * 
     * @param {array} tickers The tickers to retrieve details for
     * @param {string} currency The currency to get the details in (USD, AUD, etc.)
     * @returns {Promise}
     */
    fetchMarketDetails(tickers, currency) {
        currency = currency || 'USD';
        let currencySymbol;
        switch(currency) {
            case 'EUR':
                currencySymbol = '€';
                break;

            case 'CNY':
                currencySymbol = '¥';
                break;

            case 'GBP':
                currencySymbol = '£';
                break;

            case 'USD':
            default:
                currencySymbol = '$';
        }

        return new Promise((resolve, reject) => {
            new CoinMarketCap({convert: currency}).multi(coins => {
                let marketDetails = {};
                let currencyLower = currency.toLowerCase();
                for (let ticker of tickers) {
                    ticker = ticker.toLowerCase();
                    marketDetails[ticker] = new MarketDetail(ticker, currency, currencySymbol, coins.get(ticker));
                }

                resolve(marketDetails);
            });
        });
    }
}