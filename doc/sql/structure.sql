DROP TABLE IF EXISTS `app_config`;

CREATE TABLE `app_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app` varchar(50) NOT NULL DEFAULT '',
  `config_name` varchar(50) NOT NULL,
  `config_value` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_config_name` (`app`,`config_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `app_feedback`;

CREATE TABLE `app_feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app` varchar(50) NOT NULL, 
  `author` varchar(100) NOT NULL DEFAULT '',
  `author_email` varchar(100) DEFAULT NULL, 
  `content` text NOT NULL, 
  `created_at` datetime NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `app_telemetry`;

CREATE TABLE `app_telemetry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `donation_goal`;

CREATE TABLE `donation_goal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `goal_type` varchar(50) NOT NULL DEFAULT '',
  `position` int(10) unsigned DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `donations` int(10) unsigned NOT NULL DEFAULT '0',
  `goal_btc_total` decimal(11,8) unsigned DEFAULT NULL,
  `current_btc_total` decimal(11,8) unsigned NOT NULL DEFAULT '0.00000000',
  `btc_address` varchar(50) NOT NULL DEFAULT '',
  `btc_balance` decimal(11,8) unsigned NOT NULL DEFAULT '0.00000000',
  `bch_address` varchar(50) NOT NULL DEFAULT '',
  `bch_balance` decimal(11,8) unsigned NOT NULL DEFAULT '0.00000000',
  `ecc_address` varchar(50) NOT NULL DEFAULT '',
  `ecc_balance` decimal(16,8) unsigned NOT NULL DEFAULT '0.00000000',
  `eth_address` varchar(50) NOT NULL DEFAULT '',
  `eth_balance` decimal(11,8) unsigned NOT NULL DEFAULT '0.00000000',
  `ltc_address` varchar(50) NOT NULL DEFAULT '',
  `ltc_balance` decimal(11,8) unsigned NOT NULL DEFAULT '0.00000000',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `donation_goal_i18n`;

CREATE TABLE `donation_goal_i18n` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `donation_goal_id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `locale` varchar(10) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;