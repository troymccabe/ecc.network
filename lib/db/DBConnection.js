/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

var DonationGoal = require('../model/DonationGoal');
var mysql = require('mysql');

// create the ppol for the app
var pool = mysql.createPool({
    host: process.env.ECC_NETWORK_DB_HOST || '127.0.0.1',
    port: process.env.ECC_NETWORK_DB_PORT || 3306,
    user: process.env.ECC_NETWORK_DB_USER || 'root',
    password: process.env.ECC_NETWORK_DB_PASSWORD || '',
    database: process.env.ECC_NETWORK_DB_DB || 'ecc'
});

/**
 * Fetch donation goals from the database
 * 
 * @param {string} locale The locale to fetch goals in
 * @param {array} acceptedCryptos The accepted cryptos we'll pass to the donation goal object
 * @returns {Promise}
 */
pool.fetchDonationGoals = (locale, acceptedCryptos) => {
    return new Promise((resolve, reject) => {
        // big query, but not that scary
        // We're selecting the selected locale AND english (as a fallback)
        pool.query(
            'SELECT ' +
                '`donation_goal`.`id`, ' +
                'COALESCE(`donation_goal_i18n_locale`.`name`, `donation_goal_i18n_default`.`name`) AS \'name\', ' +
                'COALESCE(`donation_goal_i18n_locale`.`description`, `donation_goal_i18n_default`.`description`) AS \'description\', ' +
                '`donation_goal`.`goal_type`, ' +
                'CASE WHEN `donation_goal`.`expiration_date` IS NOT NULL THEN DATEDIFF(`donation_goal`.`expiration_date`, CURDATE()) ELSE NULL END AS \'days_remaining\', ' +
                '`donation_goal`.`donations`, ' +
                '`donation_goal`.`goal_btc_total`, ' +
                '`donation_goal`.`current_btc_total`, ' +
                '`donation_goal`.`btc_address`, ' +
                '`donation_goal`.`btc_balance`, ' +
                '`donation_goal`.`bch_address`, ' +
                '`donation_goal`.`bch_balance`, ' +
                '`donation_goal`.`ecc_address`, ' +
                '`donation_goal`.`ecc_balance`, ' +
                '`donation_goal`.`eth_address`, ' +
                '`donation_goal`.`eth_balance`, ' +
                '`donation_goal`.`ltc_address`, ' +
                '`donation_goal`.`ltc_balance`, ' +
                '`donation_goal`.`updated_at` ' +
            'FROM `donation_goal` ' + 
            'JOIN `donation_goal_i18n` AS `donation_goal_i18n_default` ON `donation_goal`.`id` = `donation_goal_i18n_default`.`donation_goal_id` ' +
            'AND `donation_goal_i18n_default`.`locale` = \'en\' ' +
            'LEFT JOIN `donation_goal_i18n` AS `donation_goal_i18n_locale` ON `donation_goal`.`id` = `donation_goal_i18n_locale`.`donation_goal_id` ' +
            'AND `donation_goal_i18n_locale`.`locale` = ? ' +
            'ORDER BY `donation_goal`.`position`, `donation_goal`.`goal_type`, COALESCE(`donation_goal_i18n_locale`.`name`, `donation_goal_i18n_default`.`name`)',
            [locale || 'en'],
            (error, rows, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }

                // hydrate all the DonationGoal objects
                var goals = [];
                rows.forEach(row => {
                    goals.push(new DonationGoal(row, acceptedCryptos));
                });
                resolve(goals);
            }
        );
    });
};

/**
 * Fetches the config values for a given app
 * 
 * @param {string} app The name of the app to get config for
 * @returns {Promise}
 */
pool.fetchAppConfig = (app) => {
    return new Promise((resolve, reject) => {
        pool.query(
            'SELECT `config_name`, `config_value` FROM `app_config` WHERE `app` = ?',
            [app],
            (error, rows, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }

                var config = {};
                if (rows.length > 0) {
                    rows.forEach(row => {
                        try {
                            config[row.config_name] = JSON.parse(row.config_value);
                        } catch (e) {
                            config[row.config_name] = row.config_value;
                        }
                    });
                }
                resolve(config);
            }
        )
    });
};

/**
 * Persists a config value for an app
 * 
 * @param {string} app The name of the app to update the config for
 * @param {string} configName The name of the config to update
 * @param {*} configValue The new value of the config
 * @returns {Promise}
 */
pool.persistAppConfig = (app, configName, configValue) => {
    return new Promise((resolve, reject) => {
        pool.query(
            'INSERT INTO `app_config` (`app`, `config_name`, `config_value`, `created_at`, `updated_at`) VALUES (?, ?, ?, NOW(), NOW()) ON DUPLICATE KEY UPDATE `config_value` = VALUES(`config_value`)',
            [app, configName, configValue],
            (error, rows, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }

                resolve(true);
            }
        )
    });
};

/**
 * Saves feedback from an app
 * 
 * @param {string} app The name of the app the feedback was submitted from
 * @param {string} author The name of the submitter
 * @param {string} authorEmail An email to reach the submitter
 * @param {string} content The content of the feedback
 * @returns {Promise}
 */
pool.persistAppFeedback = (app, author, authorEmail, content) => {
    return new Promise((resolve, reject) => {
        pool.query(
            'INSERT INTO app_feedback (app, author, author_email, content, created_at) VALUES (?, ?, ?, ?, NOW())',
            [app, author, authorEmail, content],
            (error, results) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }

                resolve(results);
            }
        )
    });
};

/**
 * Saves telemetry data from an app
 * 
 * @param {string} app The name of the app the data was submitted from
 * @param {string} data The data to save
 * @returns {Promise}
 */
pool.persistAppTelemetry = (app, data) => {
    return new Promise((resolve, reject) => {
        pool.query(
            'INSERT INTO app_telemetry (app, content, created_at) VALUES (?, ?, NOW())',
            [app, data],
            (error, results) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }

                resolve(results);
            }
        )
    });
};

/**
 * Saves a donation goal to the database
 * 
 * @param {DonationGoal} goal The goal to persist
 * @returns {Promise}
 */
pool.persistDonationGoal = goal => {
    return new Promise((resolve, reject) => {
        pool.query(
            'UPDATE `donation_goal` ' +
            'SET ' +
                '`donations` = ?, ' +
                '`current_btc_total` = ?, ' +
                '`btc_balance` = ?, ' +
                '`bch_balance` = ?, ' +
                '`ecc_balance` = ?, ' +
                '`eth_balance` = ?, ' +
                '`ltc_balance` = ? ' +
            'WHERE `id` = ?',
            [
                goal.donations, 
                goal.currentBTCTotal,
                goal.cryptos.btc.balance,
                goal.cryptos.bch.balance,
                goal.cryptos.ecc.balance,
                goal.cryptos.eth.balance,
                goal.cryptos.ltc.balance,
                goal.id
            ],
            (error, results) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }

                resolve(results);
            }
        )
    });
};

module.exports = pool;