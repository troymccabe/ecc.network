/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

let DonationGoal = require('../model/DonationGoal');

/** 
 * Service to fetch and persist donation goals from the database
 */
module.exports = class DonationGoalService {
    /**
     * Creates an instance for the app to work wtih
     * 
     * @param {DBConnection} dbConnection The db connection to work with
     * @param {BalanceLookupService} balanceLookupService The balance lookup service to retrieve balances
     * @param {MarketDetailService} marketDetailService The market detail service to retrieve btc equivalency
     */
    constructor(dbConnection, balanceLookupService, marketDetailService) {
        this.ACCEPTED_CRYPTOS = ['btc', 'bch', 'ecc', 'eth', 'ltc'];
        this.dbConnection = dbConnection;
        this.balanceLookupService = balanceLookupService;
        this.marketDetailService = marketDetailService;
    }

    /**
     * Fetches goals from the DB
     * 
     * @param {string} locale The locale to fetch goals in
     * @returns {Promise}
     */
    fetchDonationGoals(locale) {
        return this.dbConnection.fetchDonationGoals(locale || 'en', this.ACCEPTED_CRYPTOS);
    }

    /**
     * Fetches new balances and BTC equivalences for goals, then persists them to the database
     * 
     * @returns {Promise}
     */
    updateDonationGoals() {
        return new Promise((resolve, reject) => {
            // once we have both the goals and market details
            Promise.all([
                this.marketDetailService.fetchMarketDetails(this.ACCEPTED_CRYPTOS),
                this.fetchDonationGoals()
            ]).then(results => {
                var marketDetails = results[0];
                var goals = results[1];
                var updatePromises = [];
                
                // shoot off an update for each one
                goals.forEach(goal => {
                    updatePromises.push(this.updateDonationGoal(marketDetails, goal));
                })

                // all good?
                Promise.all(updatePromises)
                    .then(() => {
                        resolve();
                    }).catch(error => {
                        console.error(error);
                        reject(error);
                    });
            }).catch(error => {
                console.error(error);
                reject(error);
            });
        });
    }

    /**
     * Updates a single goal with new balancs to the db
     * 
     * @param {MarketDetail[]} marketDetails The market details for the accepted cryptos
     * @param {DonationGoal} goal The goal to update
     * @returns {Promise}
     */
    updateDonationGoal(marketDetails, goal) {
        return new Promise((resolve, reject) => {
            // grab all balances from this goal
            var balancePromises = [];
            for (var ticker in goal.cryptos) {
                balancePromises.push(this.fetchBalance(ticker, goal.cryptos[ticker].address));
            }
            Promise.all(balancePromises)
                // all balances came down ok
                .then((balanceResponses) => {
                    // reset the summed amounts (don't want to inflate)
                    goal.donations = 0;
                    goal.currentBTCTotal = 0;
                    var dbPromises = [];
                    balanceResponses.forEach(balanceResponse => {
                        if (balanceResponse.success) {
                            // update individual balances
                            goal.cryptos[balanceResponse.ticker].balance = balanceResponse.balance;
                            
                            // sum the donation count
                            goal.donations += balanceResponse.transactions;

                            // we have the ability to get a btc equivalence? add it
                            if (marketDetails[balanceResponse.ticker]) {
                                goal.currentBTCTotal += marketDetails[balanceResponse.ticker].btcValue * balanceResponse.balance;
                            }

                            // fire off the update query
                            dbPromises.push(this.dbConnection.persistDonationGoal(goal));
                        }
                    });
                    
                    Promise.all(dbPromises)
                        .then(() => {
                            resolve();
                        }).catch(error => {
                            console.error(error);
                            reject(error);
                        });
                }).catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    /**
     * Fetches the balance from the `BalanceLookupService`
     * 
     * @param {string} ticker The ticket to get the balance for
     * @param {string} address The address to get the balance for
     * @returns {Promise}
     */
    fetchBalance(ticker, address) {
        return new Promise((resolve, reject) => {
            this.balanceLookupService
                .fetchBalance(ticker, address)
                .then(addrDetails => {
                    addrDetails.ticker = ticker;
                    resolve(addrDetails);
                }).catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
};