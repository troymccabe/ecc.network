/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

const request = require('request');

/** 
 * Service to retrieve balance for a crypto address
 */
module.exports = class BalanceLookupService
{
    /**
     * Fetches the balance for a given address for a given currency
     * 
     * @param {string} tickerInput
     * @param {string} address 
     * @returns {Promise}
     */
    fetchBalance(tickerInput, address)
    {
        return new Promise((resolve, reject) => {
            let ticker = tickerInput.toLowerCase();
            switch (ticker) {
                case 'btc':
                case 'bch':
                    var domain = `${ticker == 'bch' ? 'bitcoincash.' : ''}blockexplorer.com`;
                    // enforce length on addresses
                    if (address.length <= 24) {
                        resolve({success: false, balance: 0, transactions: 0});
                    } else {
                        request.get(
                            `https://${domain}/api/addr/${address}`,
                            function(err, resp, body) {
                                if (err) { 
                                    console.error(err); 
                                    reject(err); 
                                }

                                try {
                                    var json = JSON.parse(body);
                                    resolve({success: true, balance: json.balance, transactions: json.txApperances});
                                } catch (err) {
                                    console.error(err);
                                    reject(err);
                                }
                            }
                        )
                    }
                    break;

                case 'eth':
                case 'ltc':
                    if (address) {
                        request.get(
                            `https://api.blockcypher.com/v1/${ticker}/main/addrs/${address}`,
                            function(err, resp, body) {
                                if (err) { 
                                    console.error(err);
                                    reject(err); 
                                }

                                try {
                                    var json = JSON.parse(body);
                                    resolve({success: true, balance: (json.final_balance || 0) * 1E-8, transactions: json.final_n_tx || 0});
                                } catch (err) {
                                    console.error(err);
                                    reject(err);
                                }
                            }
                        )
                    } else {
                        resolve({success: false, balance: 0, transactions: 0});
                    }
                    break;

                case 'ecc':
                    if (address) {
                        request.get(
                            `https://chainz.cryptoid.info/${ticker}/address.dws?${address}.htm`,
                            function(err, resp, body) {
                                if (err) {
                                    console.error(err);
                                    reject(err);
                                } else {
                                    var addressIdMatches = body.match(/addrID=\d+/igm);
                                    if (addressIdMatches) {
                                        var addressId = addressIdMatches[0].replace('addrID=', '');
                                        request.get(
                                            `https://chainz.cryptoid.info/explorer/address.summary.dws?coin=ecc&id=${addressId}&fmt.js`,
                                            function (err, resp, body) {
                                                if (err) {
                                                    console.error(err);
                                                    reject(err);
                                                } else {
                                                    try {
                                                        var json = JSON.parse(body);
                                                        resolve({ success: true, balance: (json.balance || 0) * 1E-8, transactions: json.receivednb || 0 });
                                                    } catch (err) {
                                                        console.error(err);
                                                        reject(err);
                                                    }
                                                }
                                            }
                                        );
                                    } else {
                                        console.error('Failed to extract address id');
                                        reject('Failed to extract address id');
                                    }
                                }
                            }
                        );
                    } else {
                        resolve({success: false, balance: 0, transactions: 0});
                    }
                    break;

                default:
                    reject('Ticker not suppported');
                    break;
            }
        });
    }
}