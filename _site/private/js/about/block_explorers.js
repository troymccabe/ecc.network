/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#tx-search-animation', '/js/about/block_explorers/animation/tx_search.json');
    loadAnimation('#addr-search-animation', '/js/about/block_explorers/animation/addr_search.json');
});