/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    var $appDownloadTabs = $('#app-download-container').find('.nav-tabs');
    if (navigator.platform.indexOf('Win') > -1) {
        $appDownloadTabs.find('a[href="#app-download-windows"]').tab('show');
    } else if(navigator.platform.indexOf("Mac") > -1) {
        $appDownloadTabs.find('a[href="#app-download-mac"]').tab('show');
    } else if(navigator.platform.indexOf("Linux") > -1) {
        $appDownloadTabs.find('a[href="#app-download-linux"]').tab('show');
    }
});