/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

var express = require('express');
var api = express.Router();
var fs = require('fs');
var request = require('request');

/** Retrieves the configuration for an app */
api.get('/app/:app/config', (req, res) => {
    res.header('Cache-Control', 'max-age=60, must-revalidate');
    req.deps.db.fetchAppConfig(req.params.app)
        .then(config => {res.json({success: true, config: config});})
        .catch(err => {res.json({success: false, message: 'Failed to retrieve config'});});
});

/** Captures feedback for an app */
api.post('/app/:app/feedback', (req, res) => {
    try {
        if (!req.body.author || !req.body.content) throw new Error();
        req.deps.db.persistAppFeedback(req.params.app, req.body.author, req.body.author_email, req.body.content)
            .then(() => {
                request.post({
                    url: 'http://54.245.0.121:33788',
                    headers: {"Content-Type": "application/json"},
                    body: {
                        channels: {discord: ['sapphire-feedback']}, 
                        message: `*${req.body.author}* ` + (req.body.author_email ? `<${req.body.author_email}>` : '') + `\n\n${req.body.content}`
                    },
                    json: true
                }, function (error, response, body) { });
                
                res.json({success: true});
            })
            .catch(err => {res.json({success: false});});
    } catch (e) {
        res.json({success: false});
    }
});

/** Captures telemetry data for an app */
api.post('/app/:app/telemetry', (req, res) => {
    res.header('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate');
    try {
        if (!req.body.action) throw new Error();
        req.deps.db.persistAppTelemetry(req.params.app, JSON.stringify(req.body))
            .then(config => {res.json({success: true});})
            .catch(err => {res.json({success: false});});
    } catch (e) {
        res.json({success: false});
    }
});

/** Gets block height */
api.get('/block_height', (req, res) => {
    res.header('Cache-Control', 'max-age=120, must-revalidate');
    var cacheFilename = `${__dirname}/../../../var/cache/explorer-height`;
    try {
        var cacheFileStats = fs.statSync(cacheFilename);
        // less than 2 minutes difference since last modified, still use it
        if (((new Date) - new Date(cacheFileStats.mtime)) < (1000 * 60 * 2)) {
            res.json(JSON.parse(fs.readFileSync(cacheFilename)));
        } else {
            throw new Error('expired');
        }
    } catch (err) {
        request.get('https://chainz.cryptoid.info/ecc/api.dws?q=getblockcount', (cryptoidError, cryptoidData) => {
            var cryptoidHeight = 0;
            if (cryptoidError) {
                console.error(cryptoidError);
            } else {
                cryptoidHeight = new Number(cryptoidData.body);
            }
    
            request.get('https://cryptobe.com/chain/ECC/q/getblockcount', (cryptobeError, cryptobeData) => {
                var cryptobeHeight = 0;
                if (cryptobeError) {
                    console.error(cryptobeError);
                } else {
                    // @TODO turn this back on when cryptobe has updated their daemon
                    // cryptobeHeight = new Number(cryptobeData.body);
                }
                
                var resp;
                if (cryptoidError && cryptobeError) {
                    resp = {success: false};
                } else {
                    resp = {success: true, block_height: Math.max(cryptoidHeight, cryptobeHeight)};
                }

                res.json(resp);
                fs.writeFileSync(cacheFilename, JSON.stringify(resp));
            });
        });
    }
});

/** Gets download links for a product */
api.get('/downloads/:product', (req, res) => {
    new req.deps.releaseService()
        .fetchReleaseInformation(req.params.product, req.query.version, req.query.platform)
        .then(function(releasesJson) { res.json(releasesJson); })
        .catch(function(errorJson) { res.json(errorJson) });
});

/** Gets donation goals */
api.get('/donation_goals', (req, res) => {
    new req.deps.donationGoalService(req.deps.db)
        .fetchDonationGoals()
        .then(function(goals) { res.json({success: true, goals: goals}); })
        .catch(function(error) { res.json({success: false}) });
});

module.exports = api;