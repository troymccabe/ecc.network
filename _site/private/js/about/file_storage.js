/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#security-animation', '/js/about/file_storage/animation/security.json');
    loadAnimation('#privacy-animation', '/js/about/file_storage/animation/privacy.json');
    loadAnimation('#redundancy-animation', '/js/about/file_storage/animation/redundancy.json');
    loadAnimation('#ease-of-use-animation', '/js/about/file_storage/animation/easy_to_use.json');
    loadAnimation('#host-animation', '/js/about/file_storage/animation/host.json');
});