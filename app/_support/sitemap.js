/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

/**
 * The sitemap contains the structure of the site
 * 
 * This is used to directly build the header & footer
 */
module.exports = (locale, res) => {
    return {
        /**
         * The map is the actual structure of the site. This is used for lookups in `resolve`
         */
        map: {
            news: { type: 'link', name: locale.NEWS || 'News', href: 'https://medium.com/@project_ecc/latest', target: '_blank' },
            about: {
                type: 'group',
                name: locale.ABOUT || 'About',
                links: {
                    overview: { name: locale.COIN_OVERVIEW, href: '/about/overview' },
                    ans: { name: locale.ANS, href: '/about/name_resolution' },
                    messaging: { name: locale.MESSAGING, href: '/about/messaging' },
                    fileStorage: { name: locale.FILE_STORAGE, href: '/about/file_storage' },
                    multiChainSystem: { name: locale.MULTI_CHAIN_SYSTEM, href: '/about/multi_chain' },
                    team: { name: locale.TEAM || 'Team', href: '/about/team' },
                    exchanges: { name: locale.EXCHANGES || 'Exchanges', href: '/about/exchanges' },
                    block_explorers: { name: locale.BLOCK_EXPLORERS, href: '/about/block_explorers' },
                }
            },
            connect: {
                type: 'group',
                name: locale.CONNECT || 'Connect',
                links: {
                    bitcointalk: { name: 'Bitcointalk', href: 'https://bitcointalk.org/index.php?topic=1006830', target: '_blank' },
                    contact: { type: 'link', name: locale.CONTACT, href: '/connect/contact' },
                    discord: { name: 'Discord', href: 'https://discord.gg/BYBhQVq', target: '_blank' },
                    facebook: { name: 'Facebook', href: 'https://www.facebook.com/projectECC/', target: '_blank' },
                    github: { name: 'Github', href: 'https://github.com/project-ecc/', target: '_blank' },
                    reddit: { name: 'Reddit', href: 'https://www.reddit.com/r/ecc/', target: '_blank' },
                    telegram: { name: 'Telegram', href: 'https://t.me/joinchat/F_LCJQ0vhFFknOSMgxHPAw', target: '_blank' },
                    twitter: { name: 'Twitter', href: 'https://twitter.com/project_ecc', target: '_blank' }
                }
            },
            downloads: {
                type: 'group',
                name: 'Downloads',
                links: {
                    lynx: { name: 'Lynx', href: '/downloads/lynx' },
                    sapphire: { name: 'Sapphire', href: '/downloads/sapphire' }
                }
            },
            help: {
                type: 'group',
                name: locale.HELP || 'Help',
                links: {
                    faq: { name: locale.FAQ, href: '/help/faq' },
                    wiki: { name: locale.WIKI, href: 'https://github.com/project-ecc/ECC-Wiki/wiki', target: '_blank' }
                }
            },
            donate: { type: 'link', name: locale.DONATE, href: '/donate' },
        },

        /**
         * Resolves a name to a link (if found)
         * 
         * @param {string} linkName The name of the link to look for in the `map`
         * @returns {Object} 
         */
        resolve: (linkName) => {
            var returnVal = {};
            var map = res.locals.sitemap.map;
            Object.keys(map).forEach(key => {
                if (returnVal.name) { return; }

                if (key == linkName) {
                    returnVal = map[key];
                } else if (map[key].links && map[key].links[linkName]) {
                    returnVal = map[key].links[linkName];
                } else {
                    returnVal = { name: '', href: '' };
                }
            });

            returnVal.anchorAttrs = `href="${returnVal.href}" target="${returnVal.target || '_self'}"`;
            return returnVal;
        }
    };
};
