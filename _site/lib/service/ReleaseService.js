/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

const fs = require('fs');
const request = require('request');

/** 
 * The release service retrieves details about releases from GitHub
 */
module.exports = class ReleaseService {
    /**
     * Fetches release information for a given product (with optional version and platform specified)
     * 
     * @param {string} product The product to get release info fo
     * @param {string} version The version string to retrieve detail for
     * @param {string} platform The platform string to retrieve detail for
     * @returns {Promise}
     */
    fetchReleaseInformation(product, version, platform) {
        return new Promise((resolve, reject) => {
            var requestCallback = () => {};
            var extractChecksum = (platform, text) => {
                // kinda shitty...node doesn't have dotall (s flag) as of this code...whitespace/not whitespace is a hack...
                var checksumMatches = text.match(new RegExp(`[\\s\\S]*checksum-${platform}: (\\w+)[\\s\\S]*`));
        
                return (checksumMatches && checksumMatches.length > 1) ? checksumMatches[1] : '';
            }
            var formatDownloadURL = (product, version, platform) => {
                return `https://github.com/project-ecc/${product}/releases/download/${version}/${product}-${version}-${platform}.zip`;
            }
            
            if (version) {
                requestCallback = (releases) => {
                    if (releases.length == 0) {
                        return {success: false, message: 'No releases found'};
                    }
        
                    for (var i = 0; i < releases.length; i++) {
                        var release = releases[i];
                        if (release.tag_name == version) {
                            var checksum = extractChecksum(platform, release.body);
                            if (!checksum) {
                                return {success: false, message: `No ${platform} checksum found for ${product}@${version}`};
                            }
        
                            return {
                                success: true, 
                                versions: [{
                                    name: release.tag_name,
                                    released: release.published_at, 
                                    download_url: formatDownloadURL(product, version, platform),
                                    checksum: checksum
                                }]
                            };
                        }
                    }
                }
            } else {
                requestCallback = (releases) => {
                    if (releases.length == 0) {
                        return {success: false, message: 'No releases found'};
                    }
        
                    var json = {success: true, versions: []};
                    for (var i = 0; i < releases.length; i++) {
                        var release = releases[i];

                        // don't show draft releases or prereleases, or releases without assets
                        if (release.draft ||release.prerelease || release.assets.length == 0) {
                            continue;
                        }

                        // stub the structure for "all platforms" (used for specific platforms)
                        var platformDetails = {
                            linux32: {
                                download_url: '', 
                                checksum: extractChecksum('linux32', release.body),
                                size: 0
                            }, linux64: {
                                download_url: '', 
                                checksum: extractChecksum('linux64', release.body),
                                size: 0
                            }, mac: {
                                download_url: '', 
                                checksum: extractChecksum('mac', release.body),
                                size: 0
                            }, win32: {
                                download_url: '', 
                                checksum: extractChecksum('win32', release.body),
                                size: 0
                            }, win64: {
                                download_url: '', 
                                checksum: extractChecksum('win64', release.body),
                                size: 0
                            },
                        }
                        // just grab platform names
                        var platforms = Object.keys(platformDetails);
                        
                        // walk through each asset to make sure we have all platforms accounted for
                        for (var j = 0; j < release.assets.length; j++) {
                            var asset = release.assets[j];

                            for (var k = 0; k < platforms.length; k++) {
                                // if the browser download url matches our format, grab the details and update the platform
                                var pattern = new RegExp(`.*${product}-${release.tag_name}-${platforms[k]}.zip$`, 'i');
                                if (asset.browser_download_url && pattern.test(asset.browser_download_url)) {
                                    platformDetails[platforms[k]].download_url = asset.browser_download_url;
                                    platformDetails[platforms[k]].size = asset.size;
                                }
                            }
                        }

                        var releaseToAdd = {name: release.tag_name, released: release.published_at};
                        if (platform) {
                            if (platform == '*') {
                                // all platforms must be accounted for for a release to show up in the all-platform feed
                                if (!(
                                    platformDetails.linux32.download_url && platformDetails.linux32.size &&
                                    platformDetails.linux64.download_url && platformDetails.linux64.size &&
                                    platformDetails.mac.download_url && platformDetails.mac.size &&
                                    platformDetails.win32.download_url && platformDetails.win32.size &&
                                    platformDetails.win64.download_url && platformDetails.win64.size
                                )) {
                                    continue;
                                }

                                releaseToAdd.platforms = platformDetails;
                            } else {
                                // the download details must be available for a release to show up for the selected platform
                                if (!(platformDetails[platform].download_url && platformDetails[platform].size)) {
                                    continue;
                                }

                                releaseToAdd.download_url = platformDetails[platform].download_url;
                                releaseToAdd.checksum = platformDetails[platform].checksum;
                                releaseToAdd.size = platformDetails[platform].size;
                            }
                        }
                        json.versions.push(releaseToAdd);
                    };
        
                    return json;
                }
            }

            var cacheFilename = `${__dirname}/../../var/cache/${product}-releases`;
            try {
                var cacheFileStats = fs.statSync(cacheFilename);
                // less than 60 minutes difference since last modified, still use it
                if (((new Date) - new Date(cacheFileStats.mtime)) < (1000 * 60 * 60)) {
                    resolve(requestCallback(JSON.parse(fs.readFileSync(cacheFilename))));
                } else {
                    throw new Error('expired');
                }
            } catch (err) {
                var requestOptions = {
                    url: `https://api.github.com/repos/project-ecc/${product}/releases`,
                    headers: {
                        'User-Agent': 'ecc.network'
                    }
                }
                request.get(requestOptions, (error, response, body) => {
                    if (error) {
                        console.error(error);
                        reject({ success: false, message: 'An internal error occurred' });
                    }
            
                    try {
                        resolve(requestCallback(JSON.parse(body)));
                        fs.writeFileSync(cacheFilename, body);
                    } catch (error) {
                        console.error(error);
                        reject({ success: false, message: 'An internal error occurred' });
                    }
                });
            }
        });
    }
}