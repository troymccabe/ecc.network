/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

const fs = require('fs');
const gulp = require('gulp');
const concat = require('gulp-concat');
const pump = require('pump');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');

const PUBLIC_FONT_DIR = 'public/font';
const PRIVATE_JS_DIR = 'private/js';
const PUBLIC_JS_DIR = 'public/js';
const PRIVATE_SASS_DIR = 'private/scss';
const PUBLIC_CSS_DIR = 'public/css';

gulp.task('css', function() {
    pump([
        gulp.src(PRIVATE_SASS_DIR + '/ecc.scss'),
        sass({outputStyle: 'compressed'}),
        gulp.dest(PUBLIC_CSS_DIR)
    ]);
});

gulp.task('js', function() {
    pump([
        gulp.src([
            'node_modules/jquery/dist/jquery.js',
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
            'node_modules/lottie-web/build/player/lottie.js',
            'node_modules/lottie-web/build/player/lottie.min.js',
            'node_modules/modal-video/js/jquery-modal-video.js',
            'node_modules/modal-video/js/jquery-modal-video.min.js',
        ]),
        gulp.dest(PUBLIC_JS_DIR + '/vendor'),
    ]);
    pump([
        gulp.src([PRIVATE_JS_DIR + '/*.js', PRIVATE_JS_DIR + '/**/*.js']),
        gulp.dest(PUBLIC_JS_DIR),
        uglify(),
        rename({suffix: '.min'}),
        gulp.dest(PUBLIC_JS_DIR)
    ]);
});

gulp.task('font', function() {
    pump([
        gulp.src('node_modules/font-awesome/fonts/*'),
        gulp.dest(PUBLIC_FONT_DIR)
    ]);
});

gulp.task('assets', ['css', 'js', 'font']);

gulp.task('watch', function() {
    gulp.watch(
        [
            PRIVATE_SASS_DIR + '/*.scss', 
            PRIVATE_SASS_DIR + '/**/*.scss', 
            PRIVATE_SASS_DIR + '/**/**/*.scss', 
            PRIVATE_JS_DIR + '/*.js',
            PRIVATE_JS_DIR + '/**/*.js',
            PRIVATE_JS_DIR + '/**/**/*.js'
        ], 
        ['assets']
    );
});