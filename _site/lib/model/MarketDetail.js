/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

/**
 * Represents the market detail for a cryptocurrency
 */
module.exports = class MarketDetail {
    /**
     * Creates a `MarketDetail` representation for a cryptocurrency
     * 
     * @param {string} ticker The ticker symbol of the cryptocurrency
     * @param {string} currency The fiat currency (USD, AUD, etc.)
     * @param {string} currencySymbol The fiat currency symbol ($, ¥, etc.)
     * @param {object} coinMarketDetails The CoinMarketCap response object for a cryptocurrency
     */
    constructor(ticker, currency, currencySymbol, coinMarketDetails) {
        var currencyLower = currency.toLowerCase();
        
        this.ticker = ticker
        this.currency = currency;
        this.currencySymbol = currencySymbol;
        this.price = Number(coinMarketDetails[`price_${currencyLower}`]);
        this.btcValue = Number(coinMarketDetails['price_btc']);
        this.marketCap = Number(coinMarketDetails[`market_cap_${currencyLower}`]);
        this.volume = Number(coinMarketDetails[`24h_volume_${currencyLower}`]);
    }
}