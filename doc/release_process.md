# ECC Application release process

When releasing a tool the following steps MUST be followed:

1. Create & push an annotated tag to GitHub
  * The tag message MUST have at least 5 lines which provide the checksum for each platform & architecture. e.g.:
```
checksum-win32: <checksum>
checksum-win64: <checksum>
checksum-lin32: <checksum>
checksum-lin64: <checksum>
checksum-mac: <checksum>
```
2. On that tag, there should be 5 files attached with names following the format `{product}-{version}-{platform}.zip`. For example:
   1. eccoin-v0.2.5.6-linux32.zip
   2. eccoin-v0.2.5.6-linux64.zip
   3. eccoin-v0.2.5.6-mac.zip
   4. eccoin-v0.2.5.6-win32.zip
   5. eccoin-v0.2.5.6-win64.zip

The website would have the following endpoint for tools:

`/api/v{x}/downloads/{app}`

A plain GET to this URL WILL return the list of versions and their release times

```
GET /api/v1/downloads/lynx

{success:true, versions:[{name: string, released: DateTime}, …]}
```

If you provide a platform, you get the checksum & download URL for each release (if available)

```
GET /api/v1/downloads/lynx?platform=win32

{success:true, versions:[{name: string, released: DateTime, download_url: string, checksum: sha1}, …]}
```

To retrieve a specific release, provide the version as well:

```
GET /api/v1/downloads/lynx?version=v0.1.9&platform=win32

{success:true, versions:[{name: string, released: DateTime, download_url: string, checksum: sha1}]}
```

This allows apps to check if there’s an updated version of either themselves or the daemon, and then are able to download, extract, validate checksum using GitHub’s servers for the bulk of the data. The checksums are read directly from the tag message.