/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

require('dotenv').config();

const fs = require('fs');
const s3sync = require('s3-sync');
const readdirp = require('readdirp');
const langs = require('../app/_support/langs');
const map = require('../app/_support/sitemap')({}).map;
const request = require('request');
const siteRoot = `${__dirname}/..`;
let requests = [];

function saveMarkup(requestOpts) {
    request(requestOpts, function (error, response, body) {
        if (!error) {
            var langDir = saveDir = `${siteRoot}/public/${requestOpts.lang}`;
            var pathParts = requestOpts.filename.split('/');
            var filename = pathParts.pop();
            pathParts.forEach(part => {
                saveDir = `${saveDir}/${part}`;
                if (!fs.existsSync(saveDir)) {
                    fs.mkdirSync(saveDir);
                }
            });
            fs.writeFileSync(`${langDir}${requestOpts.filename}`, body.replace(/(a href=")\//g, `$1/${requestOpts.lang}/`));
        }
    });
}

for (var lang in langs) {
    var langDir = `${siteRoot}/public/${lang}`;
    if (!fs.existsSync(langDir)) {
        fs.mkdirSync(langDir);
    }

    var jar = request.jar();
    jar.setCookie(`locale=${lang}`, 'http://localhost:33800');
    var requestOpts = { jar: jar, lang: lang, method: 'GET' };
    requestOpts.url = 'http://localhost:33800/';
    requestOpts.filename = '/index.html';
    saveMarkup(Object.assign({}, requestOpts));
    Object.keys(map).forEach(key => {
        if (map[key]['type'] == 'link') {
            requestOpts.url = `http://localhost:33800${map[key]['href']}`;
            if (map[key]['href'] == '/') {
                requestOpts.filename = '/index.html';
            } else {
                requestOpts.filename = `/${map[key]['href']}/index.html`;
            }
            saveMarkup(Object.assign({}, requestOpts));
        } else if (map[key]['type'] == 'group') {
            Object.keys(map[key]['links']).forEach(linkName => {
                linkObj = map[key]['links'][linkName];
                if (linkObj['target'] != '_blank' && !linkObj['href'].match(/^\/#/)) {
                    requestOpts.url = `http://localhost:33800${linkObj['href']}`;
                    requestOpts.filename = `/${linkObj['href']}/index.html`;
                    saveMarkup(Object.assign({}, requestOpts));
                }
            });
        }
    });
}