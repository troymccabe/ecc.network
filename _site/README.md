# ecc.network
The home page for ECC

## Install
After cloning the repository, simply run 

`npm install`

## Configure
The following environment variables are used in the application:

`ECC_NETWORK_ENV` : `'prod'|'dev'|'local'` : default:'prod' : The environment of the app. When this value is `local`, the SSL server is disabled.  
`ECC_NETWORK_DEBUG` : `bool` : default:false : Whether to run the app in debug mode or not 
`ECC_NETWORK_HTTP_PORT` : `int` : default:80 : The HTTP port to listen on  
`ECC_NETWORK_HTTPS_PORT` : `int` : default:443 : The HTTPS port to listen on   
`ECC_NETWORK_DB_HOST` : `string` : default:'localhost' : The host for the ECC db  
`ECC_NETWORK_DB_PORT` : `int` : default:3306 : The port for the ECC db  
`ECC_NETWORK_DB_USER` : `string` : default:'root' : The username for the ECC db user  
`ECC_NETWORK_DB_PASSWORD` : `string` : default:'' : The password for the ECC db user  
`ECC_NETWORK_DB_DB`: `string` : default:'ecc' : The name of the ECC db  
`ECC_NETWORK_CONTACT_EMAIL_ADDRESS` : `string` : default:'' : The address to log into the SMTP transport to send contact email  
`ECC_NETWORK_CONTACT_EMAIL_PASSWORD` : `string` : default:'' : The password for the email address to log into the SMTP transport to send contact email  

At a minimum, `ECC_NETWORK_ENV` and `ECC_NETWORK_HTTP_PORT` are required.

## Build
Gulp is used to build the assets for the site.

### Tasks
#### `css`
Compiles the css for the whole site to `ecc.css` in the publicly accessible directory. This file is minified, as it's expected that browser dev tools will be used to fiddle with styling

#### `js`
Compiles js to the publicly accessible directory, along with a minified copy. If `ECC_NETWORK_DEBUG=true`, the non-minified scripts are included on the site

#### `font`
Copies fonts from FontAwesome to the publicly accessible directory

#### `assets`
This is a combination of the 3 above tasks

#### `watch`
Watch all source asset files (js, scss) for changes and run `assets`

## Run
`npm start`
