INSERT INTO `donation_goal` (`id`, `group`, `position`, `expiration_date`, `donations`, `goal_btc_total`, `current_btc_total`, `btc_address`, `btc_balance`, `bch_address`, `bch_balance`, `eth_address`, `eth_balance`, `ltc_address`, `ltc_balance`, `created_at`, `updated_at`)
VALUES
	(1,'community',1,'2018-03-31',9,12.00000000,0.12013808,'1LC8zhYNXgRQ5d6sCTxDrC8wBq6D1gdQDZ',0.00000000,'1LC8zhYNXgRQ5d6sCTxDrC8wBq6D1gdQDZ',1.00242021,'ESnoQdpHH5vLafzj9nvXqRugPSkd2ZNrch',0.00000000,'LPADrS2UjUXjYikZd3y3jv6MeZSHP5HukT',0.49887065,'2018-03-06 02:35:10','2018-03-06 02:35:10'),
	(2,'devFund',1,'2031-01-01',0,100.00000000,0.00000000,'abc',0.00000000,'def',0.00000000,'ghi',0.00000000,'jkl',0.00000000,'2018-03-06 02:33:07','2018-03-06 02:33:07');

INSERT INTO `donation_goal_i18n` (`id`, `donation_goal_id`, `name`, `description`, `locale`, `created_at`)
VALUES
	(1,1,'Huobi','This is the goal for Huobi','en','2018-03-04 00:00:00'),
	(2,1,'Huobi','Eso es un gol por huobi','es','2018-03-04 00:00:00'),
	(3,2,'Dev fund','This is the general fund for ECC development','en','2018-03-04 00:00:00');