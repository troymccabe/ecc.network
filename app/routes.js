/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

let AWS = require('aws-sdk');
let express = require('express');
let router = express.Router();
let request = require('request');
let nodemailer = require('nodemailer');
let fs = require('fs');
let os = require('os');

const CONTACT_EMAIL_ADDRESS = process.env.ECC_NETWORK_CONTACT_EMAIL_ADDRESS;
const CONTACT_EMAIL_PASSWORD = process.env.ECC_NETWORK_CONTACT_EMAIL_PASSWORD;

AWS.config.update({ region: 'us-east-1' });

/*
 * "Home"
 */
router.get('/', function(req, res) {
    res.render('index');
});

// (Old home page)
router.get('/index', function(req, res) {
    res.redirect('/');
});

/*
 * "About"
 */
// -> "Overview"
router.get('/about/overview', (req, res) => {
    res.render('about/overview');
});

// (AJAX)
router.get('/about/market_details', function(req, res) {
    var currency = 'USD';
    switch (res.locals.lang) {
        case 'zh_hans':
        case 'zh_hant':
            currency = 'CNY';
            break;

        case 'de':
        case 'fr':
        case 'nl':
            currency = 'EUR'
            break;
    }

    var ticker = 'ecc';
    new req.deps.marketDetailService()
        .fetchMarketDetails([ticker], currency)
        .then(function(details) {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(details[ticker]));
        });
});

// -> "Multi Chain System"
router.get('/about/multi_chain', function (req, res) {
    res.render('about/multi_chain');
});

// -> "Address-Name Resolution"
router.get('/about/name_resolution', function (req, res) {
    res.render('about/name_resolution');
});

// -> "Messaging"
router.get('/about/messaging', function (req, res) {
    res.render('about/messaging');
});

// -> "File Storage"
router.get('/about/file_storage', function (req, res) {
    res.render('about/file_storage');
});

// -> "Team"
router.get('/about/team', function (req, res) {
    res.render('about/team');
});

// -> "White Paper"
router.get('/about/whitepaper/:lang?', function (req, res) {
    var lang = req.params.lang || res.locals.lang || 'en';

    if (!fs.existsSync(`${__dirname}/../public/downloads/documents/${lang}/ecc-whitepaper.pdf`)) {
        lang = 'en';
    }

    res.redirect(`/downloads/documents/${lang}/ecc-whitepaper.pdf`);
});

// -> "Exchanges"
router.get('/about/exchanges', (req, res) => {
    res.render('about/exchanges');
});

// -> "Block Explorers"
router.get('/about/block_explorers', function (req, res) {
    res.render('about/block_explorers');
});

/*
 * "Downloads"
 */
// -> "Lynx"
router.get('/downloads/lynx', function(req, res)  {
    new req.deps.releaseService()
        .fetchReleaseInformation('lynx', null, '*')
        .then(function(releasesJson) { res.render('downloads/lynx', {downloads: releasesJson.versions[0].platforms}); })
        .catch(function(errorJson) { res.render('downloads/lynx', {downloads: null}); });
});

//// -> "Mobile"
//router.get('/downloads/mobile', function(req, res)  {
//    res.render('downloads/mobile');
//});

// redirect from old structure. link was used in reddit & social posts. maintain for 6 months from 2018-05-21
router.get('/ecc/downloads/sapphire', function (req, res) {
    res.redirect('/downloads/sapphire');
});

// -> "Sapphire"
router.get('/downloads/sapphire', function(req, res)  {
    new req.deps.releaseService()
        .fetchReleaseInformation('sapphire', null, '*')
        .then(function(releasesJson) { res.render('downloads/sapphire', {downloads: releasesJson.versions[0].platforms, bypass: req.query.bypass}); })
        .catch(function(errorJson) { res.render('downloads/sapphire', {downloads: null, bypass: req.query.bypass}); });
});


/*
 * "Help"
 */
// -> "FAQ"
router.get('/help/faq', function(req, res)  {
    res.render('help/faq');
});

// -> "Wiki" (link)

/*
 * "Connect"
 */
// -> "Bitcointalk" (link)

// -> "Contact"
router.get('/connect/contact', function (req, res) {
    res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate')
    var result = '';
    if (typeof req.query.success != 'undefined') {
        result = res.locals.locale.CONTACT_SUCCESS;
    } else if (typeof req.query.failure != 'undefined') {
        result = res.locals.locale.CONTACT_ERROR;
    }
    res.render('connect/contact', { emailResult: result });
});

// (POST)
router.post('/connect/contact', function (req, res) {
    var form = req.body;

    request.post(
        'https://www.google.com/recaptcha/api/siteverify',
        {
            form: {
                secret: process.env.ECC_NETWORK_RECAPTCHA_SECRET,
                response: form['g-recaptcha-response']
            }
        },
        (error, response, body) => {
            if (error) {
                console.log('post fail', error);
                res.redirect(`/connect/contact?failure`);
            } else {
                try {
                    var bodyJSON = JSON.parse(body);
                    if (bodyJSON.success) {
                        var msg = `${form.first_name} ${form.last_name} <${form.email}>${os.EOL + os.EOL}${form.message}`;
                        var mailParams = {
                            Destination: { ToAddresses: ['info@ecc.network'] },
                            Message: {
                                Subject: { Charset: 'UTF-8', Data: 'ECC Contact Form' },
                                Body: { Html: { Charset: "UTF-8", Data: msg } }
                            },
                            Source: 'cryptounitedinternal@gmail.com',
                            ReplyToAddresses: [form.email]
                        };

                        new AWS.SES({ apiVersion: '2010-12-01' })
                            .sendEmail(mailParams)
                            .promise()
                            .then(function (data) {
                                res.redirect(`/connect/contact?success`);
                            }).catch(function (error) {
                                console.error(error);
                                res.redirect(`/connect/contact?failure`);
                            });
                    } else {
                        console.error('recaptcha fail');
                        res.redirect(`/connect/contact?failure`);
                    }
                } catch (error) {
                    console.error('recaptcha error', error);
                    res.redirect(`/connect/contact?failure`);
                }
            }
        }
    );
});
// -> "Discord" (link)
// -> "Facebook" (link)
// -> "Github" (link)
// -> "Reddit" (link)
// -> "Telegram" (link)
// -> "Twitter" (link)

/*
 * "Donate"
 */
// -> "Donate"
router.get("/donate", function (req, res) {
    res.header('Cache-Control', 'max-age=300, must-revalidate');
    new req.deps.donationGoalService(req.deps.db)
        .fetchDonationGoals()
        .then(function (goals) { res.render('donate', { goals: goals }); })
        .catch(function (error) { res.render('donate', { error: error }); });
});

router.get('/downloads/updates/eccoind-win64.exe', (req, res) => {
    res.redirect('https://github.com/project-ecc/eccoin/releases/download/v0.2.5.11/eccoind-win64.exe');
});

router.get('/admin/fDAF23kjlf9', (req, res) => {
    req.deps.db.fetchAppConfig('sapphire')
        .then(config => {
            req.deps.db.persistAppConfig('sapphire', 'messaging_preview_enabled', config.messenging_preview_enabled ? 'false' : 'true')
            res.send('done');
        })
        .catch(err => {res.send('whoops');});
});

router.get('/admin/cache_kill', (req, res) => {
    switch (req.query.file) {
        case 'eccoin-releases':
        case 'explorer-height':
        case 'lynx-releases':
        case 'sapphire-releases':
            res.send(require('fs').unlinkSync(`${__dirname}/../var/cache/${req.query.file}`) ? 'yup' : 'nah');
            break;

        default:
            res.send('nah');
            break;
    }
});

/*
 * Catchall
 */
router.get('/*', function(req, res){
   res.status(404);
   res.render('error', {res: res});
});

module.exports = router;