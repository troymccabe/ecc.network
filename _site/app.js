/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

process.title = 'ecc.network';
require('dotenv').config();

var constants = require('constants');
var fs = require('fs');
var path = require('path');
var express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var http = require('http');
var helmet = require('helmet');
var routes = require('./app/routes');
var apiV1 = require('./app/api/v1/index');
var langs = require('./app/_support/langs');
var DBConnection = require('./lib/db/DBConnection');
let BalanceLookupService = require('./lib/service/BalanceLookupService');
let DonationGoalService = require('./lib/service/DonationGoalService');
let MarketDetailService = require('./lib/service/MarketDetailService');
let ReleaseService = require('./lib/service/ReleaseService');

const DEBUG = process.env.ECC_NETWORK_DEBUG || false;
const ENV = process.env.ECC_NETWORK_ENV || 'prod';
const HTTP_PORT = process.env.ECC_NETWORK_HTTP_PORT || 80;
const HTTPS_PORT = process.env.ECC_NETWORK_HTTPS_PORT || 443;
const ONE_YEAR_MILLIS = 31536000000;
const SECRET = process.env.ECC_NETWORK_SECRET;

var app = express();

// dependencies
app.use((req, res, next) => {
    req.deps = {
        db: DBConnection,
        balanceLookupService: BalanceLookupService,
        donationGoalService: DonationGoalService,
        marketDetailService: MarketDetailService,
        releaseService: ReleaseService
    }
    next();
});

// servers
var httpServer = http.createServer(app);

// set response headers
app.use(function (req, res, next) {
    var maxAge = process.env.DEFAULT_CACHE_EXPIRATION || 3600;
    res.header('X-Permitted-Cross-Domain-Policies', 'master-only')
        .header('Cache-Control', `max-age=${maxAge}, must-revalidate`)
        .header('Pragma', 'no-cache')
        .header('Expires', '-1')
        .header('Surrogate-Control', 'no-store');

    next();
});

// page security
app.use(helmet());
app.use(helmet.hsts({
    maxAge: ONE_YEAR_MILLIS,
    includeSubdomains: true,
    force: true 
}));
app.use(helmet.hidePoweredBy({ setTo: 'Team ECC' }));
app.use(helmet.xssFilter());
app.use(helmet.contentSecurityPolicy({
    directives: {
	    defaultSrc: ["'self'"],
        scriptSrc: ["'self'", 'https://www.google.com/recaptcha/', 'https://www.gstatic.com/recaptcha/', 'code.jquery.com', 'cdn.socket.io', "'unsafe-eval'", 'cdnjs.cloudflare.com', 'ajax.googleapis.com', 'www.youtube.com', 's.ytimg.com', 'maxcdn.bootstrapcdn.com', 'www.googletagmanager.com', 'www.google-analytics.com'],
    	styleSrc: ["'self'", 'ecc.network', 'fonts.googleapis.com', 'fonts.gstatic.com', 'maxcdn.bootstrapcdn.com', 'cdnjs.cloudflare.com', "'unsafe-inline'"],
	    imgSrc: ["'self'", 'www.google-analytics.com', 'cdn-images-1.medium.com', 'thumbs.gfycat.com', 'giant.gfycat.com'],
	    fontSrc: ["'self'",'fonts.googleapis.com', 'fonts.gstatic.com', 'maxcdn.bootstrapcdn.com', 'cdnjs.cloudflare.com'],
	    objectSrc: ["'self'", 'thumbs.gfycat.com', 'giant.gfycat.com'],
	    mediaSrc: ["'self'", 'thumbs.gfycat.com', 'giant.gfycat.com'],
        childSrc: ["'self'", 'www.youtube.com'],
        frameSrc: ["'self'", 'https://www.google.com/recaptcha/', 'https://www.youtube.com/']
	}
}));

// performance
app.use(compression());

// framework setup
app.set('views', path.join(__dirname, 'views'));
app.set('port', HTTP_PORT);
app.set('view engine', 'ejs');
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({secret: SECRET, saveUninitialized: true, resave: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// set up vars for templates
app.use(function(req, res, next) {
    // Set the selected language (use query, fallback to session, fallback to english)
    res.locals.lang = req.cookies.locale || 'en';

    // Merge the selected locale onto the english file (to make it fallback to english)
    var enLocale = require(`${path.join(__dirname, 'locale')}/en.json`);
    var localeFilename = `${path.join(__dirname, 'locale')}/${res.locals.lang}.json`;
    if (res.locals.lang == 'en' || !fs.existsSync(localeFilename)) {
        res.locals.locale = enLocale;
    } else {
        res.locals.locale = require(localeFilename);
        // any values that are empty should be filled with their english equivalent
        Object.keys(res.locals.locale).forEach(stringKey => {
            if (res.locals.locale[stringKey].trim() == '') {
                res.locals.locale[stringKey] = enLocale[stringKey];
            }
        });

        // fill in any keys that don't even exist in the lang file
        //  (copy en to the empty object so we don't overwrite en for future requests)
        res.locals.locale = Object.assign(Object.assign({}, enLocale), res.locals.locale);
    }

    // The sitemap is the structure of the header/footer links (only groups are used in the footer)
    // This also has the ability to look up links. For example, a link to Discord in the template always
    //  resolves to the same place: `<%= sitemap.resolve('discord').href %>`
    res.locals.sitemap = require('./app/_support/sitemap')(res.locals.locale, res);

    // This is the list of langs we support in a `code: name` map
    res.locals.langs = langs;

    // Helper function for the templates, so we don't have to write the script tags or check for debug everywhere
    res.locals.script = asset => { 
        return `<script src="${DEBUG ? asset : asset.replace(/\.js$/, '.min.js')}"></script>`; 
    };

    next(); 
});

// mount routes
app.use('/api/v1/', apiV1);
app.use('/', routes);

// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
//  production error handler
//  no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.error(err);
    res.render('error', {res: res});
});

// spin up server
httpServer.listen(HTTP_PORT);

module.exports = app;

/*
 * Update donations every 5 minutes
 */
// setInterval(() => {
//     new DonationGoalService(DBConnection, new BalanceLookupService(), new MarketDetailService())
//         .updateDonationGoals()
//         .then(function(goals) { console.log('[DONATIONS] Updated successfully'); })
//         .catch(function(error) { console.error('[DONATIONS] Failed to update') });
// }, 300000);
