/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#mobile-currency-animation', '/js/downloads/mobile/animation/currency.json');
    loadAnimation('#mobile-messaging-animation', '/js/downloads/mobile/animation/messaging.json');
    loadAnimation('#mobile-file-storage-animation', '/js/downloads/mobile/animation/file_storage.json');
});