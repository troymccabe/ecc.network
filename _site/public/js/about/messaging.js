/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#privacy-animation', '/js/about/messaging/animation/privacy.json');
    loadAnimation('#instant-messaging-animation', '/js/about/messaging/animation/instant_messaging.json');
    loadAnimation('#file-transfer-animation', '/js/about/messaging/animation/file_transfer.json');
    loadAnimation('#uptime-animation', '/js/about/messaging/animation/uptime.json');
});